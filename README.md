# Serwis Komputerowy#

## Opis aplikacji ##

Aplikacja stworzona na potrzebny przedmiotu "Zaawansowane techniki programowania". Program pozwala na zarządzanie serwisem komputerowym. Wykorzystuje wzorce projektowe.

## Interfejs graficzny ##

![1.PNG](https://bitbucket.org/repo/Xdpo9d/images/1683106897-1.PNG)

![2.PNG](https://bitbucket.org/repo/Xdpo9d/images/1295927344-2.PNG)

![3.PNG](https://bitbucket.org/repo/Xdpo9d/images/268127345-3.PNG)