package Sprzet_FM;

/**
 * Klasa definiująca sprzęt typu Monitor
 * @author Piotr Gębala
 */
public class SprzetMonitor extends Sprzet {
    /* parametry sprzetu */
    private double cenaCale15_17 = 0.0;
    private double cenaCale19_21 = 0.0;
    private double cenaCale22_24 = 0.0;
    private double cenaCale27_max = 0.0;
    
    public SprzetMonitor() {
        typSprzetu = TypSprzetu.MONITOR;
    }

    protected void dodajMonitor() {
        System.out.println("Dodaje monitor...");
    }
    /**
     * Przeciążona metoda liczaca koszt
     */
    @Override
    public void liczKosztNaprawy() {
        koszt = cenaCale15_17 + cenaCale19_21 + cenaCale22_24 + cenaCale27_max;
    }
    
    public void setCenaCale15_17() {
        cenaCale15_17 = 50;
    }

    public void setCenaCale19_21() {
        cenaCale19_21 = 75;
    }
    
    public void setCenaCale22_24() {
        cenaCale22_24 = 85;
    }

    public void setCenaCale27_max() {
        cenaCale27_max = 100;
    }
    
    public double getCenaCale15_17() {
        return cenaCale15_17;
    }

    public double getCenaCale19_21() {
        return cenaCale19_21;
    }
    
    public double getCenaCale22_24() {
        return cenaCale22_24;
    }

    public double getCenaCale27_max() {
        return cenaCale27_max;
    }
    /**
     *
     * @return
     */
    @Override
    public Sprzet getSprzet() {
        dodajMonitor();
        return this;
    }

}
