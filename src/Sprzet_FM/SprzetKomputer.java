package Sprzet_FM;

/**
 * Klasa definiująca sprzęt typu Komputer
 * @author Piotr Gębala
 */
public class SprzetKomputer extends Sprzet {
    /* parametry sprzetu */
    private double cenaProcesor = 0.0;
    private double cenaDysk = 0.0;
    private double cenaZasilacz = 0.0;
    private double cenaPamiec = 0.0;
    private double cenaPlyta = 0.0;
    private double cenaSystemOperacyjny = 0.0;

    public SprzetKomputer() {
        typSprzetu = TypSprzetu.KOMPUTER;
    }

    protected void dodajKomputer() {
        System.out.println("Dodaje komputer...");
    }
    /**
     * Przeciążona metoda liczaca koszt
     */
    @Override
    public void liczKosztNaprawy() {
        koszt = cenaProcesor + cenaDysk + cenaZasilacz
                + cenaPamiec + cenaPlyta + cenaSystemOperacyjny;
    }

    public void setCenaProcesor() {
        cenaProcesor = 30.0;
    }

    public void setCenaDysk() {
        cenaDysk = 20.0;
    }

    public void setCenaZasilacz() {
        cenaZasilacz = 15.0;
    }

    public void setCenaPamiec() {
        cenaPamiec = 10.0;
    }

    public void setCenaPlyta() {
        cenaPlyta = 25.0;
    }

    public void setCenaSystemOperacyjny() {
        cenaSystemOperacyjny = 50.0;
    }

    public double getCenaProcesor() {
        return cenaProcesor;
    }

    public double getCenaDysk() {
        return cenaDysk;
    }

    public double getCenaZasilacz() {
        return cenaZasilacz;
    }

    public double getCenaPamiec() {
        return cenaPamiec;
    }

    public double getCenaPlyta() {
        return cenaPlyta;
    }

    public double getCenaSystemOperacyjny() {
        return cenaSystemOperacyjny;
    }

    /**
     *
     * @return
     */
    @Override
    public Sprzet getSprzet() {
        dodajKomputer();
        return this;
    }

}
