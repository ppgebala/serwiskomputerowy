package Sprzet_FM;

/**
 * Klasa definiująca sprzęt typu Drukarka
 * @author Piotr Gębala
 */
public class SprzetDrukarka extends Sprzet {
    /* parametry sprzetu */
    private double cenaTusz = 0.0;
    private double cenaToner = 0.0;
    private double cenaCzyszczenie = 0.0;
    
    public SprzetDrukarka() {
        typSprzetu = TypSprzetu.DRUKARKA;
    }

    protected void dodajDrukarka() {
        System.out.println("Dodaje drukarke...");
    }
    /**
     * Przeciążona metoda liczaca koszt
     */
    @Override
    public void liczKosztNaprawy() {
        koszt = cenaTusz + cenaToner + cenaCzyszczenie;
    }
    
    public void setCenaTusz() {
        cenaTusz = 20;
    }

    public void setCenaToner() {
        cenaToner = 30;
    }

    public void setCenaCzyszczenie() {
        cenaCzyszczenie = 50;
    }
    
    public double getCenaTusz() {
        return cenaTusz;
    }

    public double getCenaToner() {
        return cenaToner;
    }

    public double getCenaCzyszczenie() {
        return cenaCzyszczenie;
    }
    
    /**
     *
     * @return
     */
    @Override
    public Sprzet getSprzet() {
        dodajDrukarka();
        return this;
    }
}
