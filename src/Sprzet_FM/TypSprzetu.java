package Sprzet_FM;

/**
 * Typy sprzętu
 * @author Piotr Gębala
 */
public enum TypSprzetu {

    KOMPUTER, DRUKARKA, MONITOR;
}
