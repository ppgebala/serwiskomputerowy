package Sprzet_FM;

/**
 * abstrakcyjna klasa będąca prototypem sprzętu
 *
 * @author Piotr Gębala
 */
public abstract class Sprzet {

    
    /* parametry sprzetu */
    protected TypSprzetu typSprzetu;
    protected int idSprzetu;
    protected String markaSprzetu;
    protected String opisSprzetu;
    protected double koszt;

    public int getIdSprzetu() {
        return idSprzetu;
    }

    public void setIdSprzetu(int idSprzetu) {
        this.idSprzetu = idSprzetu;
    }

    public void setMarkaSprzetu(String markaSprzetu) {
        this.markaSprzetu = markaSprzetu;
    }

    public void setOpisSprzetu(String opisSprzetu) {
        this.opisSprzetu = opisSprzetu;
    }

    public TypSprzetu getTypSprzetu() {
        return typSprzetu;
    }

    public String getMarka() {
        return markaSprzetu;
    }

    public String getOpis() {
        return opisSprzetu;
    }

    public double getKoszt() {
        return koszt;
    }

    @Override
    public String toString() {
        return "\tProducent: " + markaSprzetu + "\tOpis: " + opisSprzetu + "\n";
    }

    /**
     * Metoda obliczająca koszt naprawy
     */
    public abstract void liczKosztNaprawy();

    /**
     * Metoda zwracająca dany sprzęt
     *
     * @return
     */
    public abstract Sprzet getSprzet();


}
