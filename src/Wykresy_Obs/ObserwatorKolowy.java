package Wykresy_Obs;

import Main.BazaXML;
import static Main.BazaXML.getTablicaStatystyk;
import java.util.ArrayList;
import java.util.List;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

/**
 * Klasa obserwatora rysującego wykres kołowy
 *
 * @author Piotr Gębala
 */
public class ObserwatorKolowy implements IObserwator {

    private static JFreeChart wykres;
    private static ChartFrame ramka;
    private BazaXML obserwowanaBaza;
    private List<Integer> listaWartosci = new ArrayList<Integer>();
    private static DefaultPieDataset dane = new DefaultPieDataset();

    /**
     * Konstruktor, w którym ustalane są dane do wykresu
     * @param obserwowanaBaza 
     */
    public ObserwatorKolowy(BazaXML obserwowanaBaza) {
        this.obserwowanaBaza = obserwowanaBaza;

    }

    /**
     * Metoda wypełniająca listę wartości dla wykresu danymi z bazy XML
     * @param daneWart
     */
    public void wypelnijListy(double[] daneWart) {
        for (int i = 0; i < daneWart.length; i++) {
            listaWartosci.add((int) (daneWart[i] / ceny[i]));
        }
        for (int i = 0; i < tablicaEtykiet.length; i++) {
            dane.setValue(tablicaEtykiet[i], listaWartosci.get(i));
        }
    }

    /**
     * Metoda rysująca wykres kołowy
     */
    public static void rysujWykresKolowy() {
        
        //Tworzymy wykres JFreeChart typu PieChart
        wykres = ChartFactory.createPieChart("Ilość poszczególnych usług ", dane, false, true, false);
    }
    public static void rysujRamke(){
        if (ramka != null) {
            ramka.dispose();
        }
        ramka = new ChartFrame("Ilość wykonanych usług", wykres);
        ramka.setVisible(true);
        ramka.setSize(800, 600);
        ramka.setResizable(false);
        ramka.setLocationRelativeTo(null);
    }
    /**
     * Metoda aktualizująca stan obserwatora
     * @param dane
     */
    @Override
    public void aktualizuj(double[] dane) {
        wypelnijListy(dane);
        rysujWykresKolowy();
    }
}
