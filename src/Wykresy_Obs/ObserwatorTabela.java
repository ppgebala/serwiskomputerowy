package Wykresy_Obs;

import Main.BazaXML;
import static Wykresy_Obs.IObserwator.ceny;
import static Wykresy_Obs.IObserwator.tablicaEtykiet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Klasa obserwatora rysującego tabelę
 *
 * @author Piotr Gębala
 */
public class ObserwatorTabela implements IObserwator {

    private static JTable tabela;
    private static JFrame ramka;
    private static DefaultTableModel model = new DefaultTableModel();
    private BazaXML obserwowanaBaza;
    private List<Integer> listaWartosci = new ArrayList<Integer>();
    private static JScrollPane jscroll;
    private static Object[] tabObj = new Object[tablicaEtykiet.length];
    private static int[] dane = new int[tablicaEtykiet.length];

    /**
     * Konstruktor, w którym ustalane są dane do wykresu
     *
     * @param obserwowanaBaza
     */
    public ObserwatorTabela(BazaXML obserwowanaBaza) {
        this.obserwowanaBaza = obserwowanaBaza;

    }

    /**
     * Metoda wypełniająca listę wartości dla wykresu danymi z bazy XML
     *
     * @param daneWart
     */
    public void wypelnijListy(double[] daneWart) {
        for (int i = 0; i < daneWart.length; i++) {
            listaWartosci.add((int) (daneWart[i] / ceny[i]));
        }
        for (int i = 0; i < tablicaEtykiet.length; i++) {
            tabObj[i] = new Object[]{tablicaEtykiet[i], listaWartosci.get(i)};
            dane[i] = listaWartosci.get(i);
        }
    }

    /**
     * Metoda rysująca tabelę
     */
    public static void rysujTabele() {

        tabela = new JTable(model);
        if (model.getColumnCount() == 0) {
            model.addColumn("Usługa");
            model.addColumn("Ilość");

            for (int i = 0; i < tablicaEtykiet.length; i++) {
                model.addRow((Object[]) tabObj[i]);
            }
        } else {
            for (int i = 0; i < tablicaEtykiet.length; i++) {
                model.setValueAt(dane[i], i, 1);
                model.setValueAt(tablicaEtykiet[i], i, 0);
            }
        }
    }

    public static void rysujRamke() {
        jscroll = new JScrollPane(tabela);
        if (ramka != null) {
            ramka.dispose();
        }
        ramka = new JFrame("Ilość wykonanych usług");
        ramka.add(jscroll);
        ramka.setVisible(true);
        ramka.setSize(800, 300);
        ramka.setResizable(false);
        ramka.setLocationRelativeTo(null);
    }

    /**
     * Metoda aktualizująca stan obserwatora
     *
     * @param dane
     */
    @Override
    public void aktualizuj(double[] dane) {
        wypelnijListy(dane);
        rysujTabele();
    }
}
