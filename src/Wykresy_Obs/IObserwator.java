package Wykresy_Obs;

import java.util.List;

/**
 * Interfejs obserwatora
 * @author Piotr Gębala
 */
public interface IObserwator {
    int[] ceny = {30, 15, 20, 10, 25, 50, 20, 30, 50, 50, 75, 85, 100};
    String[] tablicaEtykiet = {"Procesory", "Dyski", "Zasilacze", "Pamięci",
        "Płyty Główne", "Systemy", "Tusze", "Tonery", "Czyszczenia",
        "15-17 Cali", "19-21 Cali", "22-24 Cali", "27 Cali i większe"};
    public void aktualizuj(double[] dane);
}
