package Wykresy_Obs;

/**
 * Interfejs obiektów obserwowanych
 * @author Piotr Gębala
 */
public interface IObserwowany {

    public void dodajObserwatora(IObserwator obserwator);

    public void usunObserwatora(IObserwator obserwator);

    public void powiadomObserwatorow();
}
