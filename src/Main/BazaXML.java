package Main;

import static Main.OknoGlowne.setID;
import Sprzet_FM.SprzetDrukarka;
import Sprzet_FM.SprzetKomputer;
import Sprzet_FM.SprzetMonitor;
import Usluga_CoR.Usluga;
import Wykresy_Obs.IObserwator;
import Wykresy_Obs.IObserwowany;
import Wykresy_Obs.ObserwatorSlupkowy;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Klasa obsługująca operacja na pliku XML
 *
 * @author Piotr Gębala
 */
public class BazaXML implements IObserwowany {

    static DocumentBuilderFactory fabryka;
    static DocumentBuilder parser;
    static Document dokument;
    Element root;
    Comment komentarzRoot;
    static File plikXML;
    List<Element> listaID;

    //List<Integer> listaStatystyk = new ArrayList<Integer>();
    private static double[] tablicaStatystyk = new double[13];
    private List<IObserwator> obserwatorzy = new ArrayList<IObserwator>();

    List<Element> idXML = new ArrayList<Element>();
    List<Element> typXML = new ArrayList<Element>();
    List<Element> producentXML = new ArrayList<Element>();
    List<Element> opisXML = new ArrayList<Element>();

    List<Element> cenaProcesorXML = new ArrayList<Element>();
    List<Element> cenaDyskXML = new ArrayList<Element>();
    List<Element> cenaZasilaczXML = new ArrayList<Element>();
    List<Element> cenaPamiecXML = new ArrayList<Element>();
    List<Element> cenaPlytaXML = new ArrayList<Element>();
    List<Element> cenaSystemXML = new ArrayList<Element>();

    List<Element> cenaTuszXML = new ArrayList<Element>();
    List<Element> cenaTonerXML = new ArrayList<Element>();
    List<Element> cenaCzyszczenieXML = new ArrayList<Element>();

    List<Element> cenaMonitory1XML = new ArrayList<Element>();
    List<Element> cenaMonitory2XML = new ArrayList<Element>();
    List<Element> cenaMonitory3XML = new ArrayList<Element>();
    List<Element> cenaMonitory4XML = new ArrayList<Element>();

    List<List<Element>> listaOpcjiXML = new ArrayList<List<Element>>();
    int aktualneID = 0;

    public void piszListe() {
        for (int i = 0; i < cenaProcesorXML.size(); i++) {
            System.out.println(cenaProcesorXML.get(i).getTextContent());
        }
    }
    
    public static void setDokument(Document dokument){
        BazaXML.dokument = dokument;
    }
    
    /**
     * konstruktor klasy BazaXML
     *
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public BazaXML() throws ParserConfigurationException, SAXException, IOException {
        this.listaID = new ArrayList<Element>();
    }

    /**
     * Metoda pobierająca dane zapisane już w bazie
     *
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public static void pobierzZBazy() throws ParserConfigurationException, SAXException, IOException {
        String lancuchBazy = "";
        for (int i = 0; i < tablicaStatystyk.length; i++) {
            tablicaStatystyk[i] = 0.0;
        }
        for (int i = 0; i < dokument.getElementsByTagName("Usługa").getLength(); i++) {
            lancuchBazy += dokument.getElementsByTagName("ID").item(i).getTextContent() + ". Przyjęto: ";
            lancuchBazy += dokument.getElementsByTagName("TYP").item(i).getTextContent() + "\t Producent: ";
            lancuchBazy += dokument.getElementsByTagName("PRODUCENT").item(i).getTextContent() + "\t\nOpis: ";
            lancuchBazy += dokument.getElementsByTagName("OPIS").item(i).getTextContent() + "\n";
            lancuchBazy += "-------------------------------------- "
                    + "Rachunek został wystawiony "
                    + "--------------------------------------\n\n";
            tablicaStatystyk[0] += Double.parseDouble(dokument.getElementsByTagName("PROCESOR").item(i).getTextContent());
            tablicaStatystyk[1] += Double.parseDouble(dokument.getElementsByTagName("DYSK").item(i).getTextContent());
            tablicaStatystyk[2] += Double.parseDouble(dokument.getElementsByTagName("ZASILACZ").item(i).getTextContent());
            tablicaStatystyk[3] += Double.parseDouble(dokument.getElementsByTagName("PAMIEC").item(i).getTextContent());
            tablicaStatystyk[4] += Double.parseDouble(dokument.getElementsByTagName("PLYTA").item(i).getTextContent());
            tablicaStatystyk[5] += Double.parseDouble(dokument.getElementsByTagName("SYSTEM").item(i).getTextContent());
            tablicaStatystyk[6] += Double.parseDouble(dokument.getElementsByTagName("TUSZ").item(i).getTextContent());
            tablicaStatystyk[7] += Double.parseDouble(dokument.getElementsByTagName("TONER").item(i).getTextContent());
            tablicaStatystyk[8] += Double.parseDouble(dokument.getElementsByTagName("CZYSZCZENIE").item(i).getTextContent());
            tablicaStatystyk[9] += Double.parseDouble(dokument.getElementsByTagName("MALY").item(i).getTextContent());
            tablicaStatystyk[10] += Double.parseDouble(dokument.getElementsByTagName("SREDNI").item(i).getTextContent());
            tablicaStatystyk[11] += Double.parseDouble(dokument.getElementsByTagName("DUZY").item(i).getTextContent());
            tablicaStatystyk[12] += Double.parseDouble(dokument.getElementsByTagName("RESZTA").item(i).getTextContent());
        }
        DekoratorHistoria.setHistoriaJTA("");
//        setHistoriaJTA(lancuchBazy);
    }

    public static double[] getTablicaStatystyk() {
        return tablicaStatystyk;
    }

    /**
     * Metoda zapisująca dane w bazie
     *
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void zapiszDoBazy() throws TransformerConfigurationException, TransformerException, ParserConfigurationException, SAXException, IOException {
        plikXML = new File("src\\Main\\XMLDocument.xml");

        String s1 = null, s2 = null, s3 = null, s4 = null, s5 = null, s6 = null, s7 = null, s8 = null, s9 = null, s10 = null, s11 = null, s12 = null, s13 = null;
        int aktualneId = 0;
        for (int i = 0; i < Usluga.getListaPrzyjec().size(); i++) {
            fabryka = DocumentBuilderFactory.newInstance();
            parser = fabryka.newDocumentBuilder();
            dokument = parser.parse(plikXML);
            //System.out.println(plikXML.length());
            if (plikXML.length() < 120) {
                aktualneId = 0;
            } else {
                aktualneId = Integer.parseInt(dokument.getElementsByTagName("ID").item(dokument.getElementsByTagName("Usługa").getLength() - 1).getTextContent());
            }
            root = (Element) dokument.getFirstChild();

            listaID.add(dokument.createElement("Usługa"));
            root.appendChild(listaID.get(i));

            idXML.add(dokument.createElement("ID"));
            idXML.get(i).appendChild(dokument.createTextNode(String.valueOf(aktualneId + 1)));
            listaID.get(i).appendChild(idXML.get(i));

            typXML.add(dokument.createElement("TYP"));
            typXML.get(i).appendChild(dokument.createTextNode(String.valueOf(Usluga.getListaPrzyjec().get(i).getTypSprzetu())));
            listaID.get(i).appendChild(typXML.get(i));

            producentXML.add(dokument.createElement("PRODUCENT"));
            producentXML.get(i).appendChild(dokument.createTextNode(String.valueOf(Usluga.getListaPrzyjec().get(i).getMarka())));
            listaID.get(i).appendChild(producentXML.get(i));

            opisXML.add(dokument.createElement("OPIS"));
            opisXML.get(i).appendChild(dokument.createTextNode(String.valueOf(Usluga.getListaPrzyjec().get(i).getOpis())));
            listaID.get(i).appendChild(opisXML.get(i));

            switch (Usluga.getListaPrzyjec().get(i).getTypSprzetu()) {
                case KOMPUTER: {
                    s1 = "" + String.valueOf(((SprzetKomputer) Usluga.getListaPrzyjec().get(i)).getCenaProcesor());
                    s2 = "" + String.valueOf(((SprzetKomputer) Usluga.getListaPrzyjec().get(i)).getCenaDysk());
                    s3 = "" + String.valueOf(((SprzetKomputer) Usluga.getListaPrzyjec().get(i)).getCenaZasilacz());
                    s4 = "" + String.valueOf(((SprzetKomputer) Usluga.getListaPrzyjec().get(i)).getCenaPamiec());
                    s5 = "" + String.valueOf(((SprzetKomputer) Usluga.getListaPrzyjec().get(i)).getCenaPlyta());
                    s6 = "" + String.valueOf(((SprzetKomputer) Usluga.getListaPrzyjec().get(i)).getCenaSystemOperacyjny());
                    s7 = "0.0";
                    s8 = "0.0";
                    s9 = "0.0";
                    s10 = "0.0";
                    s11 = "0.0";
                    s12 = "0.0";
                    s13 = "0.0";
                    break;
                }
                case DRUKARKA: {
                    s1 = "0.0";
                    s2 = "0.0";
                    s3 = "0.0";
                    s4 = "0.0";
                    s5 = "0.0";
                    s6 = "0.0";
                    s7 = "" + String.valueOf(((SprzetDrukarka) Usluga.getListaPrzyjec().get(i)).getCenaTusz());
                    s8 = "" + String.valueOf(((SprzetDrukarka) Usluga.getListaPrzyjec().get(i)).getCenaToner());
                    s9 = "" + String.valueOf(((SprzetDrukarka) Usluga.getListaPrzyjec().get(i)).getCenaCzyszczenie());
                    s10 = "0.0";
                    s11 = "0.0";
                    s12 = "0.0";
                    s13 = "0.0";
                    break;
                }
                case MONITOR: {
                    s1 = "0.0";
                    s2 = "0.0";
                    s3 = "0.0";
                    s4 = "0.0";
                    s5 = "0.0";
                    s6 = "0.0";
                    s7 = "0.0";
                    s8 = "0.0";
                    s9 = "0.0";
                    s10 = "" + String.valueOf(((SprzetMonitor) Usluga.getListaPrzyjec().get(i)).getCenaCale15_17());
                    s11 = "" + String.valueOf(((SprzetMonitor) Usluga.getListaPrzyjec().get(i)).getCenaCale19_21());
                    s12 = "" + String.valueOf(((SprzetMonitor) Usluga.getListaPrzyjec().get(i)).getCenaCale22_24());
                    s13 = "" + String.valueOf(((SprzetMonitor) Usluga.getListaPrzyjec().get(i)).getCenaCale27_max());
                    break;
                }
            }

            cenaProcesorXML.add(dokument.createElement("PROCESOR"));
            cenaProcesorXML.get(i).appendChild(dokument.createTextNode(s1));
            listaID.get(i).appendChild(cenaProcesorXML.get(i));

            cenaDyskXML.add(dokument.createElement("DYSK"));
            cenaDyskXML.get(i).appendChild(dokument.createTextNode(s2));
            listaID.get(i).appendChild(cenaDyskXML.get(i));

            cenaZasilaczXML.add(dokument.createElement("ZASILACZ"));
            cenaZasilaczXML.get(i).appendChild(dokument.createTextNode(s3));
            listaID.get(i).appendChild(cenaZasilaczXML.get(i));

            cenaPamiecXML.add(dokument.createElement("PAMIEC"));
            cenaPamiecXML.get(i).appendChild(dokument.createTextNode(s4));
            listaID.get(i).appendChild(cenaPamiecXML.get(i));

            cenaPlytaXML.add(dokument.createElement("PLYTA"));
            cenaPlytaXML.get(i).appendChild(dokument.createTextNode(s5));
            listaID.get(i).appendChild(cenaPlytaXML.get(i));

            cenaSystemXML.add(dokument.createElement("SYSTEM"));
            cenaSystemXML.get(i).appendChild(dokument.createTextNode(s6));
            listaID.get(i).appendChild(cenaSystemXML.get(i));

            cenaTuszXML.add(dokument.createElement("TUSZ"));
            cenaTuszXML.get(i).appendChild(dokument.createTextNode(s7));
            listaID.get(i).appendChild(cenaTuszXML.get(i));

            cenaTonerXML.add(dokument.createElement("TONER"));
            cenaTonerXML.get(i).appendChild(dokument.createTextNode(s8));
            listaID.get(i).appendChild(cenaTonerXML.get(i));

            cenaCzyszczenieXML.add(dokument.createElement("CZYSZCZENIE"));
            cenaCzyszczenieXML.get(i).appendChild(dokument.createTextNode(s9));
            listaID.get(i).appendChild(cenaCzyszczenieXML.get(i));

            cenaMonitory1XML.add(dokument.createElement("MALY"));
            cenaMonitory1XML.get(i).appendChild(dokument.createTextNode(s10));
            listaID.get(i).appendChild(cenaMonitory1XML.get(i));

            cenaMonitory2XML.add(dokument.createElement("SREDNI"));
            cenaMonitory2XML.get(i).appendChild(dokument.createTextNode(s11));
            listaID.get(i).appendChild(cenaMonitory2XML.get(i));

            cenaMonitory3XML.add(dokument.createElement("DUZY"));
            cenaMonitory3XML.get(i).appendChild(dokument.createTextNode(s12));
            listaID.get(i).appendChild(cenaMonitory3XML.get(i));

            cenaMonitory4XML.add(dokument.createElement("RESZTA"));
            cenaMonitory4XML.get(i).appendChild(dokument.createTextNode(s13));
            listaID.get(i).appendChild(cenaMonitory4XML.get(i));
        }
        setID(aktualneId + 1);
        pobierzZBazy();
        zapiszXML();
        //piszListe();
    }

    /**
     * Metoda usuwająca dane z bazy
     *
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws TransformerException
     */
    public static void usunZPliku() throws ParserConfigurationException, SAXException, IOException, TransformerException {
        List<String> listID = new ArrayList<String>();
        plikXML = new File("src\\Main\\XMLDocument.xml");
        fabryka = DocumentBuilderFactory.newInstance();
        parser = fabryka.newDocumentBuilder();
        dokument = parser.parse(plikXML);
        for (int i = 0; i < dokument.getElementsByTagName("Usługa").getLength(); i++) {
            listID.add(dokument.getElementsByTagName("ID").item(i).getTextContent());
        }
        NodeList elementyID = dokument.getElementsByTagName("ID");
        for (int k = 0; k < listID.size(); k++) {
            for (int j = 0; j < elementyID.getLength(); j++) {
                Element element = (Element) elementyID.item(j);
                if (element.getTextContent().equals(listID.get(k))) {
                    Element rodzic = (Element) element.getParentNode();
                    rodzic.getParentNode().removeChild(rodzic);
                    break;
                }
            }
        }
        for (int i = 0; i < tablicaStatystyk.length; i++) {
            tablicaStatystyk[i] = 0.0;
        }
        zapiszXML();
    }

    /**
     * Metoda zapisująca plik XML
     *
     * @throws TransformerConfigurationException
     * @throws TransformerException
     */
    public static void zapiszXML() throws TransformerConfigurationException, TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        //transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource zrodlo = new DOMSource(dokument);
        //Result wynik = new StreamResult(System.out);
        StreamResult wynik = new StreamResult(plikXML);
        transformer.transform(zrodlo, wynik);

    }
    
    
    /**
     * Metoda dodająca obserwatora dla klasy BazaXML
     * @param obserwator 
     */
    @Override
    public void dodajObserwatora(IObserwator obserwator) {
        obserwatorzy.add(obserwator);
    }

    /**
     * Metoda powiadamiająca obserwatora o zmianie stanu bazy
     */
    @Override
    public void powiadomObserwatorow() {
        Iterator<IObserwator> aktualny = obserwatorzy.iterator();
        while (aktualny.hasNext()) {
            aktualny.next().aktualizuj(tablicaStatystyk);
        }
    }

    /**
     * Metoda usuwająca obserwatora
     * @param obserwator 
     */
    @Override
    public void usunObserwatora(IObserwator obserwator) {
        obserwatorzy.remove(obserwator);
    }
}
