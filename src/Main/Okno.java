package Main;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Piotrua
 */
public abstract class Okno extends JFrame {

    protected final JPanel panelGlownyJP = new JPanel();

    public abstract void setPanel();
}
