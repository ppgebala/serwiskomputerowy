package Main;

import static Main.BazaXML.fabryka;
import static Main.BazaXML.parser;
import static Main.BazaXML.plikXML;
import Usluga_CoR.Usluga;
import Usluga_CoR.UslugaNapraw;
import Usluga_CoR.UslugaPrzyjmij;
import Usluga_CoR.UslugaRozlicz;
import Wykresy_Obs.ObserwatorKolowy;
import Wykresy_Obs.ObserwatorSlupkowy;
import Wykresy_Obs.ObserwatorTabela;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Klasa implementująca okno programu
 *
 * @author Piotr Gębala
 */
public class OknoGlowne extends Okno implements ActionListener {
    private static String typ = "Komputer";
    /* elementy okna */
//    private final JPanel panelGlownyJP = new JPanel();
    private final JPanel panelTopJP = new JPanel();
    private final JPanel panelTytulJP = new JPanel();
    private final JPanel panelDaneJP = new JPanel();
    private final JPanel panelUslugJP = new JPanel();
//    private final JPanel panelHistoriaJP = new JPanel();
    private final JPanel panelPrzyciskiJP = new JPanel();
    /* elementy paska menu */
    private final JMenuBar pasekJMB = new JMenuBar();
    private final JMenu plikJM = new JMenu("Plik");
    private final JMenu pomocJM = new JMenu("Pomoc");
    private final JMenuItem wyjscieJMI = new JMenuItem("Wyjście");
    private final JMenuItem oProgramieJMI = new JMenuItem("O Programie");
    /* pola ogólne */
    private final String[] listaCB = {"Komputer", "Drukarka", "Monitor"};
    private JComboBox typCB;
    private static final JTextField idJTF = new JTextField("0");
    private static final JTextField markaJTF = new JTextField("Nazwa domyślna");
    private static final JTextField kosztJTF = new JTextField();
    private static final JTextField stanJTF = new JTextField();
    /* znaczniki komputera*/
    private static final JCheckBox procesorKomputerJCB = new JCheckBox();
    private static final JCheckBox dyskKomputerJCB = new JCheckBox();
    private static final JCheckBox zasilaczKomputerJCB = new JCheckBox();
    private static final JCheckBox pamiecKomputerJCB = new JCheckBox();
    private static final JCheckBox plytaKomputerJCB = new JCheckBox();
    private static final JCheckBox systemKomputerJCB = new JCheckBox();
    /* znaczniki komputera*/
    private static final JCheckBox tuszDrukarkaJCB = new JCheckBox();
    private static final JCheckBox tonerDrukarkaJCB = new JCheckBox();
    private static final JCheckBox czyszczenieDrukarkaJCB = new JCheckBox();
    /* znaczniki monitora*/
    private static final JRadioButton cale15_17MonitorJRB = new JRadioButton();
    private static final JRadioButton cale19_21MonitorJRB = new JRadioButton();
    private static final JRadioButton cale22_24MonitorJRB = new JRadioButton();
    private static final JRadioButton cale27_maxMonitorJRB = new JRadioButton();
    private static final ButtonGroup monitoryBG = new ButtonGroup();
    /* pola tekstowe */
    private static final JTextArea opisJTA = new JTextArea("Brak opisu");
//    private static final JTextArea historiaJTA = new JTextArea("Brak wpisów");
    /* przyciski */
    private final JButton przyjmijJB = new JButton("Przyjmij");
    private final JButton wyczyscJB = new JButton("Usuń historie");
    private final JButton obs1JB = new JButton("Kołowy");
    private final JButton obs2JB = new JButton("Słupkowy");
    private final JButton obs3JB = new JButton("Tabela");
    /* scrolle */
    private JScrollPane scrollOpisJSP;
//    private JScrollPane scrollHistoriaJSP;

    private OknoGlowne() {
        
    }

    public static OknoGlowne getInstance() {
        return SingletonPanelHolder.INSTANCE;
    }

    private static class SingletonPanelHolder {

        private static final OknoGlowne INSTANCE = new OknoGlowne();
    }

    /**
     * Metoda ustawiająca kolejne przejścia pomiędzy metodami w łańcuchu
     * zobowiązań oraz uruchamiająca proces usługi
     */
    public static void uruchomPrzyjecie() {
        Usluga u1 = new UslugaPrzyjmij(30);
        Usluga u2 = u1.ustawNastepnik(new UslugaNapraw(20));
        Usluga u3 = u2.ustawNastepnik(new UslugaRozlicz(10));
        u1.ustalUsluge(typ, 1);
        
    }

    /**
     * Metoda pobierająca dane początkowe z pliku
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    public static void pobierzDanePoczatkowe() throws ParserConfigurationException, SAXException, IOException {
        plikXML = new File("src\\Main\\XMLDocument.xml");
        fabryka = DocumentBuilderFactory.newInstance();
            parser = fabryka.newDocumentBuilder();
            Document dokument = parser.parse(plikXML);
            BazaXML.setDokument(dokument);
                try {
                    BazaXML.pobierzZBazy();
                } catch (        ParserConfigurationException | SAXException | IOException ex) {
                    Logger.getLogger(OknoGlowne.class.getName()).log(Level.SEVERE, null, ex);
                }
    }
    
    /* Metody set i get */
    public static void setTypSprzetu(String typSprzetu) {
        typ = typSprzetu;
    }

    public static void setStan(String stan) {
        stanJTF.setText(stan);
    }

    public static void setKosztJTF(double koszt) {
        kosztJTF.setText(String.valueOf(koszt) + " zł");
    }

    public static int getID() {
        return Integer.parseInt(idJTF.getText());
    }

    public static void setID() {
        int i = Integer.parseInt(idJTF.getText()) + 1;
        idJTF.setText(String.valueOf(i));
    }

    public static void setID(int id) {
        idJTF.setText(String.valueOf(id));
    }

    public static String getMarka() {
        return markaJTF.getText().trim();
    }
    /* pobieranie JCB komputera */

    public static boolean getProcesorKomputerJCB() {
        return procesorKomputerJCB.isSelected();
    }

    public static boolean getDyskKomputerJCB() {
        return dyskKomputerJCB.isSelected();
    }

    public static boolean getZasilaczKomputerJCB() {
        return zasilaczKomputerJCB.isSelected();
    }

    public static boolean getPamiecKomputerJCB() {
        return pamiecKomputerJCB.isSelected();
    }

    public static boolean getPlytaKomputerJCB() {
        return plytaKomputerJCB.isSelected();
    }

    public static boolean getSystemKomputerJCB() {
        return systemKomputerJCB.isSelected();
    }
    /* pobieranie JCB drukarki */

    public static boolean getTuszDrukarkaJCB() {
        return tuszDrukarkaJCB.isSelected();
    }

    public static boolean getTonerDrukarkaJCB() {
        return tonerDrukarkaJCB.isSelected();
    }

    public static boolean getCzyszczenieDrukarkaJCB() {
        return czyszczenieDrukarkaJCB.isSelected();
    }
    /* pobieranie JCB monitora */

    public static boolean getCale15_17MonitorJCB() {
        return cale15_17MonitorJRB.isSelected();
    }

    public static boolean getCale19_21MonitorJCB() {
        return cale19_21MonitorJRB.isSelected();
    }

    public static boolean getCale22_24MonitorJCB() {
        return cale22_24MonitorJRB.isSelected();
    }

    public static boolean getCale27_maxMonitorJCB() {
        return cale27_maxMonitorJRB.isSelected();
    }

    public static String getOpis() {
        return opisJTA.getText().trim();
    }

//    public static void setHistoriaJTA(String historia) {
//        historiaJTA.setText(historia);
//    }

    public static void setKomputerJCB() {
        procesorKomputerJCB.setSelected(false);
        dyskKomputerJCB.setSelected(false);
        zasilaczKomputerJCB.setSelected(false);
        pamiecKomputerJCB.setSelected(false);
        plytaKomputerJCB.setSelected(false);
        systemKomputerJCB.setSelected(false);
    }

    public static void setDrukarkaJCB() {
        tuszDrukarkaJCB.setSelected(false);
        tonerDrukarkaJCB.setSelected(false);
        czyszczenieDrukarkaJCB.setSelected(false);
    }

    public static void setMonitorJCB() {
        monitoryBG.clearSelection();
    }
    
    /**
     * Ustawienia panelu TopJP
     */
    private void setPanelTopJP() {
        panelTopJP.setLayout(new GridLayout(2, 1));
        
        panelTopJP.add(panelTytulJP);
        panelTopJP.add(panelUslugJP);
    }
    /**
     * Ustawienia panelu TytulJP
     */
    private void setPanelTytulJP() {
        panelTytulJP.setLayout(new GridLayout(1, 3));
        /* pola tekstowe */
        
        opisJTA.setLineWrap(true);
        opisJTA.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent evt) {
                if (opisJTA.getText().length() >= 200) {
                    evt.consume();
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        scrollOpisJSP = new JScrollPane(opisJTA);
        panelTytulJP.add(panelDaneJP);
        panelTytulJP.add(Box.createGlue());
        panelTytulJP.add(scrollOpisJSP);
    }
    /**
     * Ustawienia panelu DaneJP
     */
    private void setPanelDaneJP() {
        panelDaneJP.setLayout(new GridLayout(6, 1));
        typCB = new JComboBox(listaCB);
        typCB.setSelectedIndex(0);
        typCB.addActionListener(this);
        idJTF.setEditable(false);
        markaJTF.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent evt) {
                if (markaJTF.getText().length() >= 40) {
                    evt.consume();
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        panelDaneJP.add(typCB);
        panelDaneJP.add(new JLabel("ID:\t"));
        panelDaneJP.add(idJTF);
        panelDaneJP.add(new JLabel("Producent:\t"));
        panelDaneJP.add(markaJTF);
    }
        /**
     * Ustawienia panelu UslugJP
     */
    private void setPanelUslugJP() {
        panelUslugJP.setLayout(new GridLayout(0, 6));
        
        tuszDrukarkaJCB.setEnabled(false);
        tonerDrukarkaJCB.setEnabled(false);
        czyszczenieDrukarkaJCB.setEnabled(false);

        monitoryBG.add(cale15_17MonitorJRB);
        monitoryBG.add(cale19_21MonitorJRB);
        monitoryBG.add(cale22_24MonitorJRB);
        monitoryBG.add(cale27_maxMonitorJRB);
        cale15_17MonitorJRB.setEnabled(false);
        cale19_21MonitorJRB.setEnabled(false);
        cale22_24MonitorJRB.setEnabled(false);
        cale27_maxMonitorJRB.setEnabled(false);
        /* rozmieszczanie elementow panelUslugJP*/
        panelUslugJP.add(new JLabel("KOMPUTERY\t"));
        panelUslugJP.add(Box.createGlue());
        panelUslugJP.add(new JLabel("DRUKARKI\t"));
        panelUslugJP.add(Box.createGlue());
        panelUslugJP.add(new JLabel("MONITORY\t"));
        panelUslugJP.add(Box.createGlue());            
        
        panelUslugJP.add(new JLabel("Procesor\t"));
        panelUslugJP.add(procesorKomputerJCB);
        panelUslugJP.add(new JLabel("Tusz\t"));
        panelUslugJP.add(tuszDrukarkaJCB);
        panelUslugJP.add(new JLabel("15\" - 17\"\t"));
        panelUslugJP.add(cale15_17MonitorJRB);

        panelUslugJP.add(new JLabel("Dysk:\t"));
        panelUslugJP.add(dyskKomputerJCB);
        panelUslugJP.add(new JLabel("Toner\t"));
        panelUslugJP.add(tonerDrukarkaJCB);
        panelUslugJP.add(new JLabel("19\" - 21\"\t"));
        panelUslugJP.add(cale19_21MonitorJRB);

        panelUslugJP.add(new JLabel("Zasilacz:\t"));
        panelUslugJP.add(zasilaczKomputerJCB);
        panelUslugJP.add(new JLabel("Czyszczenie\t"));
        panelUslugJP.add(czyszczenieDrukarkaJCB);
        panelUslugJP.add(new JLabel("22\" - 24\"\t"));
        panelUslugJP.add(cale22_24MonitorJRB);

        panelUslugJP.add(new JLabel("Pamięć:\t"));
        panelUslugJP.add(pamiecKomputerJCB);
        panelUslugJP.add(Box.createGlue());
        panelUslugJP.add(Box.createGlue());
        panelUslugJP.add(new JLabel("27\" i większe\t"));
        panelUslugJP.add(cale27_maxMonitorJRB);

        panelUslugJP.add(new JLabel("Płyta główna:\t"));
        panelUslugJP.add(plytaKomputerJCB);
        panelUslugJP.add(Box.createGlue());
        panelUslugJP.add(Box.createGlue());
        panelUslugJP.add(Box.createGlue());
        panelUslugJP.add(Box.createGlue());

        panelUslugJP.add(new JLabel("System operacyjny:\t"));
        panelUslugJP.add(systemKomputerJCB);
        panelUslugJP.add(Box.createGlue());
        panelUslugJP.add(Box.createGlue());
        panelUslugJP.add(Box.createGlue());
        panelUslugJP.add(Box.createGlue());
    }
    /**
     * Ustawienia panelu HistoriaJP
     */
//    private void setPanelHistoriaJP() {
//        panelHistoriaJP.setLayout(new GridLayout(1, 1));
//        historiaJTA.setEditable(false);
//        scrollHistoriaJSP = new JScrollPane(historiaJTA);
//        panelHistoriaJP.add(scrollHistoriaJSP);
//    }
    /**
     * Ustawienia panelu PrzyciskiJP
     */
    
    private void setPanelPrzyciskiJP() {
        panelPrzyciskiJP.setLayout(new GridLayout(3, 3));
        kosztJTF.setEditable(false);
        stanJTF.setEditable(false);
        
        przyjmijJB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (procesorKomputerJCB.isSelected()
                        || dyskKomputerJCB.isSelected()
                        || zasilaczKomputerJCB.isSelected()
                        || pamiecKomputerJCB.isSelected()
                        || plytaKomputerJCB.isSelected()
                        || systemKomputerJCB.isSelected()
                        || tuszDrukarkaJCB.isSelected()
                        || tonerDrukarkaJCB.isSelected()
                        || czyszczenieDrukarkaJCB.isSelected()
                        || cale15_17MonitorJRB.isSelected()
                        || cale19_21MonitorJRB.isSelected()
                        || cale22_24MonitorJRB.isSelected()
                        || cale27_maxMonitorJRB.isSelected()) {
                    uruchomPrzyjecie();
                } else {
                    java.awt.Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(panelGlownyJP,
                            "Nie zaznaczono żadnej opcji!", "Błąd", 0);
                }
                
            }
        });
        
        wyczyscJB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    BazaXML.usunZPliku();
                } catch (        ParserConfigurationException | SAXException | IOException | TransformerException ex) {
                    Logger.getLogger(OknoGlowne.class.getName()).log(Level.SEVERE, null, ex);
                }
                DekoratorHistoria.setHistoriaJTA("");
//                setHistoriaJTA("");
            }
        });
        
        
        obs1JB.addActionListener(new ActionListener() {
            /**
             * obsługa przycisku, dodanie obserwatora dla wykresu kołowego
             * @param e 
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                ObserwatorKolowy.rysujWykresKolowy();
                ObserwatorKolowy.rysujRamke();
            }
        });
        
        
        obs2JB.addActionListener(new ActionListener() {
            /**
             * obsługa przycisku, dodanie obserwatora dla wykresu słupkowego
             * @param e 
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                ObserwatorSlupkowy.rysujWykresSlupkowy();
                ObserwatorSlupkowy.rysujRamke();
            }
        });
        
        
        obs3JB.addActionListener(new ActionListener() {
            /**
             * obsługa przycisku, dodanie obserwatora dla tabeli
             * @param e 
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                ObserwatorTabela.rysujTabele();
                ObserwatorTabela.rysujRamke();
            }
        });
        
        panelPrzyciskiJP.add(przyjmijJB);
        panelPrzyciskiJP.add(obs1JB);
        panelPrzyciskiJP.add(wyczyscJB);

        panelPrzyciskiJP.add(new JLabel("Stan:\t"));
        panelPrzyciskiJP.add(obs2JB);
        panelPrzyciskiJP.add(new JLabel("Koszt:\t"));

        panelPrzyciskiJP.add(stanJTF);
        panelPrzyciskiJP.add(obs3JB);
        panelPrzyciskiJP.add(kosztJTF);
    }
    /**
     * Metoda definiująca ustawienia okna oraz obsługe przycisków
     */
    public void oknoProgramu() {
        /* ustawienia okna */
        panelGlownyJP.setLayout(new BorderLayout());
        setContentPane(panelGlownyJP);
        setPanelTopJP();
        setPanelTytulJP();
        setPanelDaneJP();
        setPanelUslugJP();
//        setPanelHistoriaJP();
        setPanelPrzyciskiJP();
        this.setSize(800, 600);
        this.setTitle("Serwis Komputerowy");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        /* ustawienia paska menu */
        setJMenuBar(pasekJMB);
        pasekJMB.add(plikJM);
        pasekJMB.add(pomocJM);
        /* obsługa przycisków */
        wyjscieJMI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.equals(e) == true) {
                    System.exit(0);
                }
            }
        });
        oProgramieJMI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.equals(e) == true) {
                    JOptionPane.showMessageDialog(panelGlownyJP,
                            "Aplikacja serwisu komputerowego\n"
                            + "ZTP Laboratorium 3", "Informacja", 1);

                }
            }
        });
        plikJM.add(wyjscieJMI);
        pomocJM.add(oProgramieJMI);
//        panelGlownyJP.add(panelTopJP, BorderLayout.NORTH);
////        panelGlownyJP.add(panelHistoriaJP, BorderLayout.CENTER);
//        panelGlownyJP.add(Box.createGlue(), BorderLayout.CENTER);
//        panelGlownyJP.add(panelPrzyciskiJP, BorderLayout.SOUTH);
        setPanel();
        setVisible(true);
    }
    
    @Override
    public void setPanel() {
        panelGlownyJP.add(panelTopJP, BorderLayout.NORTH);
//        panelGlownyJP.add(panelHistoriaJP, BorderLayout.CENTER);
        panelGlownyJP.add(Box.createGlue(), BorderLayout.CENTER);
        panelGlownyJP.add(panelPrzyciskiJP, BorderLayout.SOUTH);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox cb = (JComboBox) e.getSource();
        String typSprzetu = (String) cb.getSelectedItem();
        setTypSprzetu(typSprzetu);
        switch (typSprzetu) {
            case "Komputer": {
                procesorKomputerJCB.setEnabled(true);
                dyskKomputerJCB.setEnabled(true);
                zasilaczKomputerJCB.setEnabled(true);
                pamiecKomputerJCB.setEnabled(true);
                plytaKomputerJCB.setEnabled(true);
                systemKomputerJCB.setEnabled(true);

                tuszDrukarkaJCB.setEnabled(false);
                tonerDrukarkaJCB.setEnabled(false);
                czyszczenieDrukarkaJCB.setEnabled(false);

                cale15_17MonitorJRB.setEnabled(false);
                cale19_21MonitorJRB.setEnabled(false);
                cale22_24MonitorJRB.setEnabled(false);
                cale27_maxMonitorJRB.setEnabled(false);

                setDrukarkaJCB();
                setMonitorJCB();
                break;
            }
            case "Drukarka": {
                procesorKomputerJCB.setEnabled(false);
                dyskKomputerJCB.setEnabled(false);
                zasilaczKomputerJCB.setEnabled(false);
                pamiecKomputerJCB.setEnabled(false);
                plytaKomputerJCB.setEnabled(false);
                systemKomputerJCB.setEnabled(false);

                tuszDrukarkaJCB.setEnabled(true);
                tonerDrukarkaJCB.setEnabled(true);
                czyszczenieDrukarkaJCB.setEnabled(true);

                cale15_17MonitorJRB.setEnabled(false);
                cale19_21MonitorJRB.setEnabled(false);
                cale22_24MonitorJRB.setEnabled(false);
                cale27_maxMonitorJRB.setEnabled(false);
                setKomputerJCB();
                setMonitorJCB();
                break;
            }
            case "Monitor": {
                procesorKomputerJCB.setEnabled(false);
                dyskKomputerJCB.setEnabled(false);
                zasilaczKomputerJCB.setEnabled(false);
                pamiecKomputerJCB.setEnabled(false);
                plytaKomputerJCB.setEnabled(false);
                systemKomputerJCB.setEnabled(false);

                tuszDrukarkaJCB.setEnabled(false);
                tonerDrukarkaJCB.setEnabled(false);
                czyszczenieDrukarkaJCB.setEnabled(false);

                cale15_17MonitorJRB.setEnabled(true);
                cale19_21MonitorJRB.setEnabled(true);
                cale22_24MonitorJRB.setEnabled(true);
                cale27_maxMonitorJRB.setEnabled(true);

                setKomputerJCB();
                setDrukarkaJCB();
                break;
            }
        }
    }

}
