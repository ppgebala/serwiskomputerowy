package Main;

import static Main.OknoGlowne.pobierzDanePoczatkowe;
import Usluga_CoR.UslugaNapraw;
import static Usluga_CoR.UslugaNapraw.aktualizujBaze;
import static Usluga_CoR.UslugaNapraw.obserwujFasada;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 * Klasa główna programu
 *
 * @author Piotr Gębala
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     * @throws javax.xml.transform.TransformerException
     */
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {
        OknoGlowne okno = OknoGlowne.getInstance();
//        Okno okno = new DekoratorHistoria(new OknoGlowne());
        okno.oknoProgramu();
        pobierzDanePoczatkowe();
        aktualizujBaze();
        obserwujFasada();
    }
}
