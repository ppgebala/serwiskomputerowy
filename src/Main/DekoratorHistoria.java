package Main;

import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Piotrua
 */
public class DekoratorHistoria extends Dekorator {

    private final Okno okno;
    private final JPanel panelHistoriaJP = new JPanel();
    private static final JTextArea historiaJTA = new JTextArea("Brak wpisów");
    private JScrollPane scrollHistoriaJSP;

    public DekoratorHistoria(Okno okno) {
        this.okno = okno;
    }

    public static void setHistoriaJTA(String historia) {
        historiaJTA.setText(historia);
    }

    private void setPanelHistoriaJP() {
        panelHistoriaJP.setLayout(new GridLayout(1, 1));
        historiaJTA.setEditable(false);
        scrollHistoriaJSP = new JScrollPane(historiaJTA);
        panelHistoriaJP.add(scrollHistoriaJSP);
    }
    
    @Override
    public void setPanel() {
        panelGlownyJP.add(panelHistoriaJP);
    }
}
