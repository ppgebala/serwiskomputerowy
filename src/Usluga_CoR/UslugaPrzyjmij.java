package Usluga_CoR;

import static Main.OknoGlowne.getCale15_17MonitorJCB;
import static Main.OknoGlowne.getCale19_21MonitorJCB;
import static Main.OknoGlowne.getCale22_24MonitorJCB;
import static Main.OknoGlowne.getCale27_maxMonitorJCB;
import static Main.OknoGlowne.getCzyszczenieDrukarkaJCB;
import static Main.OknoGlowne.getDyskKomputerJCB;
import static Main.OknoGlowne.getID;
import static Main.OknoGlowne.getMarka;
import static Main.OknoGlowne.getOpis;
import static Main.OknoGlowne.getPamiecKomputerJCB;
import static Main.OknoGlowne.getPlytaKomputerJCB;
import static Main.OknoGlowne.getProcesorKomputerJCB;
import static Main.OknoGlowne.getSystemKomputerJCB;
import static Main.OknoGlowne.getTonerDrukarkaJCB;
import static Main.OknoGlowne.getTuszDrukarkaJCB;
import static Main.OknoGlowne.getZasilaczKomputerJCB;
import static Main.OknoGlowne.setID;
import Sprzet_FM.SprzetDrukarka;
import Sprzet_FM.SprzetKomputer;
import Sprzet_FM.SprzetMonitor;
import Sprzet_FM.TypSprzetu;
import Wykresy_Obs.ObserwatorKolowy;

/**
 * Klasa reprezentująca usługę przyjmowania
 * Pobierane są tutaj dane z panelu
 * a nastepnie na ich podstawie tworzone są obiekty klasy Sprzet
 * @author Piotr Gębala
 */
public class UslugaPrzyjmij extends Usluga {

    public UslugaPrzyjmij(int klucz) {
        this.klucz = klucz;
    }
    /**
     * Metoda wykonująca operacja charakterystyczne dla metody
     * @param msg 
     */
    @Override
    protected void wykonajUsluge(String msg) {
        setID();
        podzespoly = "";
        switch (msg) {
            case "Komputer": {
                sprzet = fabryka.produkujSprzet(TypSprzetu.KOMPUTER);
                sprzet.getSprzet();
                sprzet.setIdSprzetu(getID());
                sprzet.setMarkaSprzetu(getMarka());
                if (getProcesorKomputerJCB() == true) {
                    ((SprzetKomputer) sprzet).setCenaProcesor();
                }
                if (getDyskKomputerJCB() == true) {
                    ((SprzetKomputer) sprzet).setCenaDysk();
                }
                if (getZasilaczKomputerJCB() == true) {
                    ((SprzetKomputer) sprzet).setCenaZasilacz();
                }
                if (getPamiecKomputerJCB() == true) {
                    ((SprzetKomputer) sprzet).setCenaPamiec();
                }
                if (getPlytaKomputerJCB() == true) {
                    ((SprzetKomputer) sprzet).setCenaPlyta();
                }
                if (getSystemKomputerJCB() == true) {
                    ((SprzetKomputer) sprzet).setCenaSystemOperacyjny();
                }
                sprzet.setOpisSprzetu(getOpis());
                break;
            }
            case "Drukarka": {
                sprzet = fabryka.produkujSprzet(TypSprzetu.DRUKARKA);
                sprzet.getSprzet();
                sprzet.setIdSprzetu(getID());
                sprzet.setMarkaSprzetu(getMarka());
                if (getTuszDrukarkaJCB() == true) {
                    ((SprzetDrukarka) sprzet).setCenaTusz();
                }
                if (getTonerDrukarkaJCB() == true) {
                    ((SprzetDrukarka) sprzet).setCenaToner();
                }
                if (getCzyszczenieDrukarkaJCB() == true) {
                    ((SprzetDrukarka) sprzet).setCenaCzyszczenie();
                }
                sprzet.setOpisSprzetu(getOpis());
                break;
            }
            case "Monitor": {
                sprzet = fabryka.produkujSprzet(TypSprzetu.MONITOR);
                sprzet.getSprzet();
                sprzet.setIdSprzetu(getID());
                sprzet.setMarkaSprzetu(getMarka());
                if (getCale15_17MonitorJCB() == true) {
                    ((SprzetMonitor) sprzet).setCenaCale15_17();
                }
                if (getCale19_21MonitorJCB() == true) {
                    ((SprzetMonitor) sprzet).setCenaCale19_21();
                }
                if (getCale22_24MonitorJCB() == true) {
                    ((SprzetMonitor) sprzet).setCenaCale22_24();
                }
                if (getCale27_maxMonitorJCB() == true) {
                    ((SprzetMonitor) sprzet).setCenaCale27_max();
                }
                sprzet.setOpisSprzetu(getOpis());
                break;
            }
        }

        listaPrzyjec.add(sprzet);
    }
}
