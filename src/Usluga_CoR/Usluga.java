package Usluga_CoR;

import FabrykaSprzetu_FM.FabrykaSprzetu;
import FabrykaSprzetu_FM.IFabrykaSprzetu;
import Main.DekoratorHistoria;
import static Main.OknoGlowne.setID;
import Sprzet_FM.Sprzet;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstrakcyjna metoda będąca prototypem usługi
 * @author Piotr Gębala
 */
public abstract class Usluga {

    protected static List<Sprzet> listaPrzyjec = new ArrayList<Sprzet>();
    public static int IDPrzyjecie = 30;
    public static int IDNaprawa = 20;
    public static int IDRozliczenie = 10;
    protected static String daneUslugi = "";
    protected static String podzespoly = "";
    protected int klucz;
    protected Usluga przekazanie;
    protected IFabrykaSprzetu fabryka = new FabrykaSprzetu();
    protected static Sprzet sprzet;
    /**
     * Metoda służąca do ustalenia która usługa będzie następna
     * @param usluga
     * @return 
     */
    public Usluga ustawNastepnik(Usluga usluga) {
        przekazanie = usluga;
        return usluga;
    }

    public static List<Sprzet> getListaPrzyjec() {
        return listaPrzyjec;
    }
    /**
     * Metoda sprawdzająca czy usługa może obsłużyć żądanie
     * Sprawdzanie odbywa się na podstawie zadanego priorytety
     * przesłanego wraz z rodzajem żądania
     * @param wiadomosc
     * @param priorytet 
     */
    public void ustalUsluge(String wiadomosc, int priorytet) {
        if (priorytet <= klucz) {
            wykonajUsluge(wiadomosc);
        }
        if (przekazanie != null) {
            przekazanie.ustalUsluge(wiadomosc, priorytet);
        }
    }

    public static void wyczyscHistorie() {
        daneUslugi = "";
        setID(0);
//        setHistoriaJTA(daneUslugi);
        DekoratorHistoria.setHistoriaJTA(daneUslugi);
    }
    /**
     * Abstrakcyjna metoda obsługująca zdażenie charakterystyczne dla danej usługi
     * @param wiadomosc 
     */
    abstract protected void wykonajUsluge(String wiadomosc);
}
