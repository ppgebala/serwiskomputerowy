package Usluga_CoR;

import Main.BazaXML;
import Strukturalne.Fasada;
import Wykresy_Obs.ObserwatorKolowy;
import Wykresy_Obs.ObserwatorSlupkowy;
import Wykresy_Obs.ObserwatorTabela;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 * Klasa reprezentująca usługę naprawiania Liczony jest tutaj koszt naprawy oraz
 * wykonany jest zapis danych do pliku
 *
 * @author Piotr Gębala
 */
public class UslugaNapraw extends Usluga {

    private static BazaXML baza = null;

    public UslugaNapraw(int klucz) {
        this.klucz = klucz;
    }

    public static void aktualizujBaze() {
        try {
            baza = new BazaXML();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(UslugaNapraw.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Dodawanie obserwatorów przy pomocy wzorca Fasad
     */
    public static void obserwujFasada() {
        Fasada fasada = new Fasada(baza);
        fasada.obserwujKolowy();
        fasada.obserwujSlupkowy();
        fasada.obserwujTabela();
        baza.powiadomObserwatorow();
    }

    /**
     * Metoda wykonująca operacja charakterystyczne dla metody
     *
     * @param msg
     */
    @Override
    protected void wykonajUsluge(String msg) {
        sprzet.liczKosztNaprawy();
        aktualizujBaze();
        try {
            baza.zapiszDoBazy();
        } catch (TransformerException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(UslugaNapraw.class.getName()).log(Level.SEVERE, null, ex);
        }
        obserwujFasada();

    }

    public static BazaXML getBaza() {
        return baza;
    }
}
