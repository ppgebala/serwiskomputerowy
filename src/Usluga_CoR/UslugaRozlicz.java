package Usluga_CoR;

import static Main.OknoGlowne.setKosztJTF;
import static Main.OknoGlowne.setStan;

/**
 * Klasa reprezentująca usługę rozliczania
 * Tutaj ustalany jest koszt usługi
 * oraz wypisywany jest jej stan
 * @author Piotr Gębala
 */
public class UslugaRozlicz extends Usluga {

    public UslugaRozlicz(int klucz) {
        this.klucz = klucz;
    }
    /**
     * Metoda wykonująca operacja charakterystyczne dla metody
     * @param msg 
     */
    @Override
    protected void wykonajUsluge(String msg) {
        setStan("Usługa dla: [" + msg + "] ZREALIZOWANA!");
        setKosztJTF(sprzet.getKoszt());
       
    }
}
