package Strukturalne;

import Main.BazaXML;
import Wykresy_Obs.ObserwatorKolowy;
import Wykresy_Obs.ObserwatorSlupkowy;
import Wykresy_Obs.ObserwatorTabela;

/**
 *
 * @author Piotrua
 */
public class Fasada {

    private BazaXML baza;
    private ObserwatorSlupkowy obserwatorSlupkowy;
    private ObserwatorKolowy obserwatorKolowy;
    private ObserwatorTabela obserwatorTabela;

    public Fasada(BazaXML baza) {
        this.baza = baza;
        obserwatorKolowy = new ObserwatorKolowy(baza);
        obserwatorSlupkowy = new ObserwatorSlupkowy(baza);
        obserwatorTabela = new ObserwatorTabela(baza);
    }

    public void obserwujKolowy() {

        baza.dodajObserwatora(obserwatorKolowy);
    }

    public void obserwujSlupkowy() {

        baza.dodajObserwatora(obserwatorSlupkowy);
    }

    public void obserwujTabela() {

        baza.dodajObserwatora(obserwatorTabela);
    }
}
