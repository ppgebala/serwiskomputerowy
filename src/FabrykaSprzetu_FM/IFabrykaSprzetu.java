package FabrykaSprzetu_FM;

import Sprzet_FM.Sprzet;
import Sprzet_FM.TypSprzetu;

/**
 * Interfejs będący prototypem fabryki sprzętu
 * @author Piotr Gębala
 */
public interface IFabrykaSprzetu {

    Sprzet produkujSprzet(TypSprzetu typSprzetu);
}
