package FabrykaSprzetu_FM;

import Sprzet_FM.Sprzet;
import Sprzet_FM.SprzetDrukarka;
import Sprzet_FM.SprzetKomputer;
import Sprzet_FM.SprzetMonitor;
import Sprzet_FM.TypSprzetu;
import Wykresy_Obs.ObserwatorKolowy;

/**
 * Klasa produkująca sprzęd w zależności od podanego typu
 * @author Piotr Gębala
 */
public class FabrykaSprzetu implements IFabrykaSprzetu {
    
    /**
     * Metoda zwracająca Sprzęt w zależności od jego typu
     * @param typSprzetu
     * @return 
     */
    @Override
    public Sprzet produkujSprzet(TypSprzetu typSprzetu) {

        Sprzet sprzet = null;

        switch (typSprzetu) {
            case KOMPUTER: {
                sprzet = new SprzetKomputer();
                break;
            }
            case DRUKARKA: {
                sprzet = new SprzetDrukarka();
                break;
            }
            case MONITOR: {
                sprzet = new SprzetMonitor();
                break;
            }
        }
        return sprzet;
    }
}
